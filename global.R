
#  ------------------------------------------------------------------------
#
#  Cartes Orients - Global
#
#  ------------------------------------------------------------------------



# Packages ----------------------------------------------------------------

library(htmltools)
library(bslib)
library(shinyWidgets)
library(phosphoricons)
library(R.utils)
library(leaflet)
library(data.table)
library(dplyr)
library(leaflet.extras2)
library(rnaturalearth)
library(rnaturalearthdata)
library(apexcharter)
library(reactable)
library(tidyr)
library(writexl)
library(pushbar)
library(sf)


# Fonctions & modules -----------------------------------------------------

R.utils::sourceDirectory("functions/", encoding = "UTF-8")
R.utils::sourceDirectory("modules/", encoding = "UTF-8")



# Datas -------------------------------------------------------------------

contours_monde <- readRDS(file = "datas/contours_monde.rds")
table_pays <- sf::st_drop_geometry(contours_monde)
geo <- yaml::read_yaml(file = "datas/geoareas.yml")

