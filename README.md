# Les Orients en cartes

<!-- badges: start -->
<!-- badges: end -->

Code source de l'application "Les Orients en cartes" réalisée par l'AFD.

<a href="https://www.afd.fr/" target="_blank">
<img src="docs/media/logo-afd.png" height="96" alt="Logo AFD"/>
</a>


## Cartes

Toutes les cartes de ce site reprennent la projection [Robinson](https://fr.wikipedia.org/wiki/Projection_de_Robinson). Cette projection est reconnue pour refléter plus fidèlement la géographie des territoires présentés.

Les contours et limites physiques des pays proviennent de [Natural Earth](https://www.naturalearthdata.com/).


## Données

La source des données est précisée pour chaque indicateur en dessous des cartes et graphiques.

Le site repose notamment sur la plateforme [DBnomics](https://db.nomics.world/) pour visualiser les données de nombreux fournisseurs avec des mises à jour automatiques et régulières.

<a href="https://db.nomics.world/" target="_blank">
<img src="docs/media/logo-dbnomics.png" height="72" alt="Logo DBnomics"/>
</a>


Certaines données n'étant pas disponibles dans DBnomics, elles ont fait l'objet d'un traitement spécifique et ne sont donc pas mises à jour automatiquement. En voici les sources :

* Classification des pays en fonction de leur revenu : [Banque mondiale](https://www.worldbank.org/en/home)
* Liberté dans le monde : [Freedom House](https://freedomhouse.org/)
* Vulnérabilité climatique Ndgain : [Ndgain](https://gain.nd.edu/)


## Réalisation

Ce site a été réalisé par [dreamRs](https://www.dreamrs.fr/) pour l'Agence française de développement.

Son code source est distribué sous licence libre AGPLv3 à l'adresse suivante : https://gitlab.com/cartorients/cartorients.

Vous pouvez à tout moment signaler un problème à l'adresse suivante : https://gitlab.com/cartorients/cartorients/-/issues.


## Vie privée

Ce site est respectueux de votre vie privée et ne collecte aucune donnée à caractère personnel.



## Mentions légales

#### Édition

Agence française de développement    
775 665 599    
5 rue Roland Barthes    
75 598 PARIS Cedex 12    
+33 (0)1 53 44 31 31

#### Conception, création graphique, développement et suivi technique

dreamRs    
830 186 276    
67 rue Rennequin    
75017 PARIS


#### Hébergement

shinyapps.io    
RStudio    
250 Northern Ave, Boston, MA 02210    
844-448-1212



## Fichier indicateurs

Le fichier Excel "datas/table_indicateurs_source.xlsx" contient des paramètres associés aux indicateurs présentés dans l'application.

Certains de ces paramètres peuvent être modifiés, voici la liste complètes des informations du fichier :

 * **id** : Identifiant de l'indicateur, *ne pas modifier*
 * **name_fr** : Nom de l'indicateur (en français)
 * **name_en** : Nom de l'indicateur (en anglais)
 * **file** : Nom du fichier contenant les données, *ne pas modifier*
 * **legend_fr** : Titre de la légende (en français)
 * **legend_en** : Titre de la légende (en anglais)
 * **unit_fr** : Unité de l'indicateur (en français)
 * **unit_en** : Unité de l'indicateur (en anglais)
 * **category_fr** : Catégorie de l'indicateur (en français)
 * **category_en** : Catégorie de l'indicateur (en anglais)
 * **code_iso** : Code ISO permettant d'identifier les pays dans les données de l'indicateur, *ne pas modifier*
 * **palette** : Palette de couleurs pour la carte, les palettes utilisables sont celles de [colorbrewer2.org](https://colorbrewer2.org/)
 * **type_palette** : Type de palette utilisée pour construire la légende : `seq`, `div`, `qual` ou `custom`. Peut être suffixé par `_rev` pour inverser l'ordre des couleurs
 * **description_fr** : Description de l'indicateur (en français)
 * **description_en** : Description de l'indicateur (en anglais)
 * **source_fr** : Source de l'indicateur (en français)
 * **source_en** : Source de l'indicateur (en anglais)
 * **annee_min** : Année minimale à afficher dans l'application
 * **annee_max** : Année maximale à afficher dans l'application



## Développement

Installation des dépendances dans R :

```r
library(pacman)
p_install_version("shinyWidgets", "0.7.0")
p_load(htmltools,
       bslib,
       shinyWidgets,
       phosphoricons,
       R.utils,
       leaflet,
       data.table,
       dplyr,
       leaflet.extras2,
       rnaturalearth,
       rnaturalearthdata,
       apexcharter,
       reactable,
       tidyr,
       writexl,
       pushbar,
       sf)
```

Cloner ce dépôt de code avec Git, puis dans R, à la racine du répertoire utiliser :

```r
shiny::runApp()
```
