
#' Paramètres pour la carte
#'
#' @param id Identifiant du module.
#'
#' @return
#'  * UI: tags HTML pouvant être inclus dans l'interface.
#'  * Server: fonction à appeler dans le server de l'application.
#' @export
#' @name parametres-carte
parametres_carte_ui <- function(id) {
  ns <- NS(id)
  tagList(
    selecteur_annee(ns("annee")),
    apexchartOutput(outputId = ns("chart_top")),
    bouton_export_xl(outputId = ns("export_global"))
  )
}

#' @param langue_r Fonction [reactive()] retournant le code de la langue à utiliser.
#'
#' @export
#' @rdname parametres-carte
parametres_carte_server <- function(id, indicateur_id_r, vision_r, langue_r = reactive("fr")) {
  moduleServer(
    id,
    function(input, output, session) {

      annee_rv <- reactiveValues(x = NULL)

      # Lecture des donnees de l'indicateur selectionne
      indicateur_r <- reactive({
        req(indicateur_id_r())
        donnees <- lire_indicateur(indicateur_id_r(), langue = langue_r())
        annee_rv$x <- maj_selecteur_annee(
          inputId = "annee",
          valeurs = donnees$year,
          indicateur = indicateur_id_r(),
          selectionnee = "recente",
          langue = langue_r()
        )
        return(donnees)
      })

      observeEvent(input$annee, annee_rv$x <- input$annee)

      # Selection d'une annee pour l'indicateur
      indicateur_annee_r <- reactive({
        req(indicateur_r(), annee_rv$x)
        indicateur_r() %>%
          filtrer_annee(annee_rv$x) %>%
          filtrer_vision(vision_r())
      })


      # Graphique top indicateur ----
      output$chart_top <- renderApexchart({
        req(indicateur_annee_r()) %>%
          barchart(langue = langue_r())
      })


      # Export global ----
      output$export_global <- downloadHandler(
        filename = function() {
          "export_global.xlsx"
          },
        content = function(file) {
          export_donnees(
            path = file,
            donnees = ajouter_libelle_pays(
              donnees = indicateur_annee_r(),
              langue = langue_r()
            ),
            langue = langue_r()
          )
        }
      )

      # Langue ----
      observeEvent(langue_r(), {
        maj_selecteurs_langue(langue_r())
      })


      # Sortir du module
      return(list(
        indicateur_r = indicateur_r,
        indicateur_annee_r = indicateur_annee_r,
        annee_r = reactive(input$annee)
      ))
    }
  )
}
