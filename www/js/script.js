/*jshint
  browser:true,
  devel: true
*/
/*global LeafletWidget */

(function() {
  $(document).on("click", '[data-bs-toggle="popover"]', function() {
     $(this).popover({sanitize: false, html: true});
     $(this).popover("show");
   });
  LeafletWidget.methods.isInitilialized = function(options) {
    (function(){
      Shiny.setInputValue(options.inputId, true);
    }).call(this);
  };
  LeafletWidget.methods.addZoom = function(options) {
    (function(){
      this.zoomControl.remove();
      L.control.zoom({ position: options.position }).addTo(this);
    }).call(this);
  };
  LeafletWidget.methods.addAttribution = function(options) {
    (function(){
      this.attributionControl.setPrefix(options.text);
    }).call(this);
  };
  LeafletWidget.methods.setPadding = function(options) {
    (function(){
      L.Path.CLIP_PADDING = 1;
      var map = this;
      map.getRenderer(map).options.padding = 1;
    }).call(this);
  };
  LeafletWidget.methods.addCustomContent = function(options) {
    (function(){
      var map = this;
      if (options.id) {
        var id = options.id;
        if (map[id]) {
          map[id].remove();
          delete map[id];
        }
        map[id] = L.control.custom(options);
        map.controls.add(map[id]);
      } else {
        map.customContent = L.control.custom(options);
        map.controls.add(map.customContent);
      }
    }).call(this);
  };
  $(document).on("click", function() {
    $(".landing-page").fadeOut(300, function() { $(this).remove(); });
  });
  $(document).on("click", ".toggle-pushbar", function(e) {
    if ($("#parameters").hasClass("opened")) {
      $(".map-container").css("left", 0);
    } else {
      $(".map-container").css("left", 350);
    }
  });
})();
