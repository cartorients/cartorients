
#' Initialisation de la carte
#'
#' @param carte Carte.
#' @param inputId Id de l'initialisation.
#'
#' @return
#' @export
#'
#' @examples
#' carte_relief_init <- carte_relief_init %>%
#' carte_initialisation(inputId = initialisationInputId)
carte_initialisation <- function(carte, inputId) {
  invokeMethod(carte, NULL, "isInitilialized", list(inputId = inputId))
}


#' Ajouter un bouton de zoom
#'
#' @param carte Carte.
#' @param position Position du zoom.
#'
#' @return Un bouton de zoom sur la carte.
#' @export
#'
ajouter_zoom <- function(carte, position = c("topright", "bottomright", "bottomleft", "topleft")) {
  position <- match.arg(position)
  invokeMethod(carte, NULL, "addZoom", list(position = position))
}



#' Ajouter un texte d'attribution
#'
#' @param carte Carte.
#' @param texte Texte d'atribution.
#'
#' @return Un texte en bas à droite sur la carte.
#' @export
#'
ajouter_attribution <- function(map, texte) {
  invokeMethod(map, NULL, "addAttribution", list(text = texte))
}


#' Ajouter un titre
#'
#' @param carte Carte.
#' @param texte Texte du titre.
#' @param className Nom de la classe.
#' @param position Position du titre.
#' @param ... Paramêtre(s) supplémentaire(s).
#' @param id Id de l'element.
#'
#' @return Un titre
#' @export
#'
#' @examples
ajouter_titre <- function(carte,
                          texte = NULL,
                          className = "info panel-map-title",
                          position = c("topleft", "topright", "bottomright", "bottomleft"),
                          ...,
                          id = NULL) {
  position <- match.arg(position)
  content <- htmltools::doRenderTags(texte)
  addControl(map = carte, html = content, position = position, className = "info panel-map-title", layerId = id, ...)
}


#' Ajouter un graticule
#'
#' @param carte Carte.
#' @param color Couleur.
#' @param weight Taille.
#'
#' @return L'ajout d'un graticule à la carte.
#' @export
#'
#' @examples
#' leaflet() %>%
#' addTiles() %>%
#' ajouter_graticule()
ajouter_graticule <- function(carte, color = "#808080", weight = 0.5) {
  addPolylines(
    map = carte,
    data = sf::st_graticule(),
    color = color,
    weight = weight
  )
}


#' Ajouter un planisphère
#'
#' @param carte Carte.
#' @param color Couleur.
#'
#' @return L'ajout d'un planisphère à la carte.
#' @export
#'
#' @examples
#' leaflet() %>%
#' addTiles() %>%
#' ajouter_planisphere()
ajouter_planisphere <- function(carte, color = "#b3cde3") {
  x <- c(rep(180, 37), seq(from = 180, to = -180, length.out = 37), rep(-180, 37), seq(from = -180, to = 180, length.out = 37))
  y <- c(seq(from = -90, to = 90, length.out = 37), rep(90, 37),  seq(from = 90, to = -90, length.out = 37), rep(-90, 37))
  background <- matrix(c(x, y), ncol = 2)
  background <- sf::st_polygon(x = list(background))
  addPolygons(
    map = carte,
    data = background,
    color = color,
    fillColor = color,
    opacity = 1,
    fillOpacity = 1,
    noClip = TRUE,
    highlightOptions = highlightOptions(bringToFront = FALSE, sendToBack = TRUE)
  )
}


#' Définition des limites de la carte
#'
#' @param map Carte.
#' @param data Données.
#'
#' @return L'ajustement des limites de la carte.
#' @export
#'
#' @examples
fit_to_bbox <- function(map, data = getMapData(map)) {
  if (!inherits(data, "sf"))
    stop("fit_to_bbox: data must be an 'sf' object", call. = FALSE)
  bbox <- sf::st_bbox(data)
  bbox <- as.list(bbox)
  fitBounds(
    map = map,
    lng1 = bbox$xmin,
    lat1 = bbox$ymin,
    lng2 = bbox$xmax,
    lat2 = bbox$ymax
  )
}


#' Définition du vol vers une limite donnée de la carte
#'
#' @param map Carte.
#' @param data Données.
#'
#' @return L'ajustement du vol vers une limite donnée de la carte.
#' @export
#'
#' @examples
fly_to_bbox <- function(map, data = getMapData(map)) {
  if (!inherits(data, "sf"))
    stop("fit_to_bbox: data must be an 'sf' object", call. = FALSE)
  bbox <- sf::st_bbox(data)
  bbox <- as.list(bbox)
  flyToBounds(
    map = map,
    lng1 = bbox$xmin,
    lat1 = bbox$ymin,
    lng2 = bbox$xmax,
    lat2 = bbox$ymax
  )
}


#' Créer une palette de couleur pour la carte
#'
#' @param donnees Données qui seront représentées sur la carte.
#' @param indicateur Identifiant de l'indicateur.
#'
#' @return Une fonction pour créer la palette.
#' @export
#'
#' @examples
#' donnees <- lire_indicateur("pib")
#' creer_palette(donnees, "pib")
creer_palette <- function(donnees, indicateur) {

  type_palette <- chercher_type_palette(indicateur)

  if (type_palette == "seq") {
    if (all(is.na(donnees$value))) {
      pal <- colorNumeric(
        palette = chercher_palette(indicateur),
        domain = NULL,
        na.color = "#cccccc"
        )
      } else {
        pal <- colorNumeric(
          palette = chercher_palette(indicateur),
          domain = range(donnees$value, na.rm = TRUE),
          na.color = "#cccccc"
        )
      }
  } else if (type_palette == "qual") {
    pal <- colorFactor(
      palette = chercher_palette(indicateur),
      domain = NULL,
      na.color = "#cccccc"
    )
  } else if (type_palette == "div_rev") {
    pal <- colorNumeric(
      palette = chercher_palette(indicateur),
      domain = NULL,
      reverse = TRUE,
      na.color = "#cccccc"
    )
  } else if (type_palette == "custom") {
    pal <- colorFactor(
      palette = strsplit(x = chercher_palette(indicateur), split = ";")[[1]],
      domain = NULL,
      levels = levels(donnees$value),
      na.color = "#cccccc"
    )
  } else {
    pal <- colorNumeric(
      palette = chercher_palette(indicateur),
      domain = c(-1 * max(abs(donnees$value), na.rm = TRUE), max(abs(donnees$value), na.rm = TRUE)),
      na.color = "#cccccc"
    )
  }
  return(pal)
}


#' Formater les données de la légende de la carte
#'
#' @param type_palette Type de palette de l'indicateur.
#' @param chiffre Données à formater.
#'
#' @return Les données de la légende de la carte formatées.
#' @export
#'
#' @examples
#' format_legende(langue = "fr")(type = "numeric", valeur = 1234)
format_legende <- function(langue = c("fr", "en")) {

  langue <- match.arg(langue)

  function(type, valeur) {

    if (type %in% c("numeric")) {
      formatter_chiffre(valeur, langue)
    } else {
      valeur
    }
  }
}
